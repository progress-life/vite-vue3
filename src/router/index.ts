import { createRouter, createWebHashHistory, RouteRecordRaw, createWebHistory } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: 'Home',
    component: () => import('@/views/home.vue')
  },
  // { path: '/', redirect: { name: 'Home' } },
  { path: '/', component: () => import('@/views/home.vue') },
  {
    path: '/about',
    name: 'About',
    component: () => import('@/views/about.vue')
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
